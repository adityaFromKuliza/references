import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './navsidebar.css';
import mystyles from './mystyle.module.css';
import GoalsOfBPMN from './GoalsOfBPMN';
import { FlowObjects } from './FlowObjects';
import { Events } from './Events';
import { BenifitsOfBPMN } from './BenifitsOfBPMN';
import { BPMN2 } from './BPMN2.0';
import { TasksActivities } from './TasksActivities';
import { SubProcesses } from './SubProcesses';
import { Gateways } from './Gateways';
import { Swimlanes } from './Swimlanes';
export const Home = (props) => (
  <div>
    
    <div id = "scroll-nav" className={mystyles.sidenav}>
    <h5>BPMN</h5>
    <a href="#bpmn">BPMN</a>
    <a href="#goals-of-bpmn">Goals Of BPMN</a>
    <a href="#benifits-of-bpmn">Benifits Of BPMN</a>
    <a href="#bpmn-2">BPMN 2.0</a>
    <a href="#flow-objects">Flow Objects</a>
    <a href="#events">Events</a>
    <a href="#tasks-activities">Tasks/Activities</a>
    <a href="#sub-processes">Sub-Processes</a>
    <a href="#gateways">Gateways</a>
    <a href="#token-concept">Token Concept</a>
    <a href="#swimlanes">Swimlanes</a>
    <a href="#connecting-objects">Connecting Objects</a>
    <a href="#data">Data</a>
    </div>
      
  <div className={mystyles.contentWrapper}>

    {/* <div className = "col-md-8"> */}
     <h4 className="col-md-8" id="bpmn">Business Process Model and Notation (BPMN)</h4>
     <p className="col-md-8">
     A business process is a set of activities that are performed in coordination in an 
     organisational and technical environment. 
     These organisations need to make sure that their 
     business processes are correctly functioning and are aligned 
     with their strategic goals. This is achieved through Business Process Management (BPM), 
     a disciplined approach to manage, execute, identify, measure, monitor, and control both 
     automated and non-automated business processes to achieve targets. Standard notations or 
     graphics are used to capture business processes, popularly Business Process Model and Notation.
     </p>
     <p className="col-md-8">
     Business Process Model and Notation (BPMN) is a graphical representation for framing business 
     processes. It visually depicts a detailed and informed sequence of business activities needed 
     to achieve a business objective. Originally developed by the Business Process Management 
     Initiative (BMPI), BPMN has been maintained by the Object Management Group (OMG) since the 
     two organizations merged in 2005.
       </p>
       <p className="col-md-8">
       Knowing how the business operates is first and the most critical step of business process 
       improvement thus, BPMN provides businesses with the ability to understand their internal 
       business procedures through graphical signs or symbols. It also gives the organizations the 
       ability to communicate these procedures in a standard manner. Furthermore, the graphical 
       notation simplifies the understanding of the business collaborations and transactions between 
       the organizations. 
        </p>
        <p className="col-md-8">
        Therefore, we understand from the above statements that BPMN is keen on providing a 
        notation that is easily understandable by all business users, from the business analysts 
        that create initial drafts of the processes, to the tech developers responsible for 
        application of the technology which execute these processes, and finally, to  people who 
        will monitor those processes.
        </p>
        <GoalsOfBPMN></GoalsOfBPMN>
        <BenifitsOfBPMN></BenifitsOfBPMN>
        <BPMN2/>
        <FlowObjects/>
        <Events></Events>
        <TasksActivities/>
        <SubProcesses/>
        <Gateways/>
        <Swimlanes/>
        </div>
        </div>
  
)