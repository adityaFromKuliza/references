import React from 'react';
import Compiler from './Compiler';

class Conditions extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkedValue:'',
            quizAnswer:window.parent.execution.getVariable("loanAmount")+"_"+window.parent.execution.getVariable("income"),
            right:'none',
            wrong:'none',
        };
        this.check = this.check.bind(this);
    }
    check(event){
        this.setState({right:'none',wrong:'none'});
        this.setState({checkedValue:event.target.value});
        if(event.target.value==this.state.quizAnswer){
        this.setState({right:'inline'});
        }else{
            this.setState({wrong:'inline'});
        }
    }
    render(){
        const exercise='Print the Rate of Interest based on the following conditions.\n'+
        'a.If loan amount is less that 50000 and income is less than 500000,then rate of interest is 12%\n'+
        'b.If loan amount is less than 50000 and income is greater than eqauls to 500000,then rate of interest is 10%.\n'+
        'c.If loan amount is greater than and equals to 50000 and income is less than 500000, then rate of interest is 14%.\n'+
        'variable names are loanAmount,income,rateOfInterest';
        const javascriptCode='var x=execution.getVariable("x");\n'+
        'var y = execution.getVariable("y");\n'+
        'var z = execution.getVariable("z");\n'+
        'if(x>y && x>z){\n\t'+
        'console.log("x is greater than y");\n'+
        '}\n'+
        'else if(x>z){\n\tconsole.log("x is greater than z")\n}\n'+
        'else if(z<x && z>x){\n\tconsole.log("z is less than y but greater than x");\n}\n'+
        'else if {x==y==z}{\n\tconsole.log("all three are same");\n}';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="conditions">
        <h4>Conditions</h4>
        <p>
        An operator is a symbol which performs certain operations.For example "<i>+</i>" is an operator
        which performs addition. While this same operator can be used for string concatenation.
<br />
 </p>
 <Compiler exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 <p>Quiz</p>what is the value of loanAmount and income?
 <ol type="a">
 <li>
     <input checked={this.state.checkedValue==='2356_3456'} type="radio" value="2356_3456" onChange={this.check}/>2356 and 3456
     <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='2356_3456'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
     </li>
     <li>
         <input checked={this.state.checkedValue==='4567_2345'} type="radio" value="4567_2345" onChange={this.check} />4567 and 2345
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='4567_2345'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
         </li>
     <li>
         <input  checked={this.state.checkedValue==this.state.quizAnswer} type="radio" value={this.state.quizAnswer} onChange={this.check} />{window.parent.execution.getVariable("loanAmount")+" and "+window.parent.execution.getVariable("income")}
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:(this.state.checkedValue==this.state.quizAnswer)?this.state.right:'none'}}>
      <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>
         </li>
 </ol>
    </div>
        );
    }
    


}
export default Conditions;