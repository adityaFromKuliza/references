import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import GoalsOfBPMN from 'components/bpmn/GoalsOfBPMN';
import { BenifitsOfBPMN } from 'components/bpmn/BenifitsOfBPMN';
import { BPMN2 } from 'components/bpmn/BPMN2.0';
import { FlowObjects } from 'components/bpmn/FlowObjects';
import { Events } from 'components/bpmn/Events';
import Compiler from 'components/javascript/Compiler';
import history from './History';
import { Home } from 'components/bpmn/Home';

import Dashboard from "components/dashboard/Dashboard";
import { JavascriptIntroduction } from "components/javascript/JavascriptIntroduction";
export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                <Route exact path="/" component={Dashboard} />
                {/* bpmn routes */}
                    <Route exact path="/bpmn" component={Home} />
                    <Route path="/goals-of-bpmn" component={GoalsOfBPMN} />
                    <Route path="/benifits-of-bpmn" component={BenifitsOfBPMN} />
                    <Route path="/bpmn-2.0" component={BPMN2} />
                    <Route path="/flow-objects" component={FlowObjects} />
                    <Route path="/events" component={Events} />

                    {/* javascript routes */}
                    <Route path="/javascript" component={JavascriptIntroduction} />
                    <Route path="/try-it-out" component={Compiler} />
                </Switch>
            </Router>
        )
    }
}