import React from 'react';
import Compiler from './Compiler';

class Operators extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkedValue:'',
            quizAnswer:window.parent.execution.getVariable("noOfMovieTheaters")*window.parent.execution.getVariable("noOfSeats"),
            right:'none',
            wrong:'none',
        };
        this.check = this.check.bind(this);
    }
    check(event){
        this.setState({right:'none',wrong:'none'});
        this.setState({checkedValue:event.target.value});
        if(event.target.value==this.state.quizAnswer){
        this.setState({right:'inline'});
        }else{
            this.setState({wrong:'inline'});
        }
    }
    render(){
        const exercise='A cineplex has few movie theaters stored in global variable "noOfMovieTheaters". If the number of seats in each theater is stored in global variable "noOfSeats", then calculate the total number of seats in the cineplex?'+
        'If the number of movies released is stored in global variable "noOfMoviesReleased",then calculate the number of movies released in each theater.';
        const javascriptCode='var newVariable_1=execution.getVariable("globalVariable_1");\n'+
        'var newVariable_2 = newVariable_1*12;\t//multiplication\n'+
        'execution.setVariable("globalVariable_2",newVariable_2);\n'+
        'var newVariable_3 = execution.getVariable("globalVariable_3");\n'+
        'var newVariable_4=z+20;\t//addition\n'+
        'var newVariable_5=newVariable_4/3;\t//division\n'+
        'execution.setVariable("globalVariable_4",newVariable_5);\n'+
        'console.log("Value of newVariable_5 is "+newVariable_5);';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="operators">
        <h4>Operators</h4>
        <p>
        An operator is a symbol which performs certain operations.
        There are various other operators,however for this course we will be considering only <i>addition(+)</i>,<i>subtraction(-)</i>,
        <i>multiplication(*) </i> and <i>division(/)</i>
<br />
 </p>
 <Compiler textAreaKey="txt-area-operators" exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 <p>Quiz</p>How many total number of seats are there?
 <ol type="a">
        <li>
            <input checked={this.state.checkedValue==='23566'} type="radio" value="23566" onChange={this.check} />23566
            <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='23566'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
            </li>
     <li>
         <input checked={this.state.checkedValue==this.state.quizAnswer} type="radio" value={this.state.quizAnswer} onChange={this.check} />{this.state.quizAnswer}
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:(this.state.checkedValue==this.state.quizAnswer)?this.state.right:'none'}}>
      <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>
         </li>
     <li><input checked={this.state.checkedValue==='6574'} type="radio" value="6574" onChange={this.check} />6574
     <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='6574'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
     </li>
 </ol>
    </div>
        );
    }
    


}
export default Operators;