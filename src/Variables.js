import React from 'react';
import Compiler from './Compiler';

class Variables extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkedValue:'',
            quizAnswer:window.parent.execution.getVariable("applicantIncome"),
            right:'none',
            wrong:'none',
        };
        this.check = this.check.bind(this);
    }
    check(event){
        this.setState({right:'none',wrong:'none'});
        this.setState({checkedValue:event.target.value});
        if(event.target.value==this.state.quizAnswer){
        this.setState({right:'inline'});
        }else{
            this.setState({wrong:'inline'});
        }
    }
    render() {
        const exercise='The value of income is stored in a global variable "applicantIncome".'+
        'Fetch this income value and assign it to a new variable "incomeDeclared".'+ 
        'Print the value of "incomeDeclared" on the console';
        
        const javascriptCode='var newVariable=execution.getVariable("globalVariable");\n'+
        'console.log("Value of newVariable "+newVariable);';
        const note='To view the output press F12 and select the console tab.';
       
        return (
            <div className="col-md-8" id="variables">
                <h4>Variables</h4>
                <p>
                    In script task when you want to get or fetch the value of any global variable <code>execution.getVariable(global_variable_name)</code> is used.
            Similarly, for setting a value of a global variable <code>execution.setVariable(global_variable_name,value)</code> is used.
    <br />
                </p>
                 <Compiler exercise={exercise} javascriptCode={javascriptCode}
                    note={note}
                ></Compiler> 
                 <p>Quiz</p>What was the value of "incomeDeclared"?
 <ol type="a">
        <li><input checked={this.state.checkedValue==this.state.quizAnswer} type="radio" onChange={this.check} value={this.state.quizAnswer}/>&nbsp;{this.state.quizAnswer}
        
        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:(this.state.checkedValue==this.state.quizAnswer)?this.state.right:'none'}}>
      <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>

        </li>
     <li><input checked={this.state.checkedValue==='900'} type="radio" value="900" onChange={this.check} />&nbsp;900
     

      <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='900'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
     
     </li>
     <li><input checked={this.state.checkedValue==='600'} type="radio" value="600" onChange={this.check} />&nbsp;600
     
      <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='600'?this.state.wrong:'none'}}>
       <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
      </svg>

     </li>
 </ol>
            </div>
        );
    }


}
export default Variables;