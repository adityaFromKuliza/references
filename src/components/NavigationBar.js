import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
const Styles = styled.div`
  .navbar { 
    background-color: #333;
    overflow: hidden;
    top:0;
    position:fixed;
    width:100%;
    z-index:1;
    
  }
  a, .navbar-nav, .navbar-light .nav-link {
    overflow-x: hidden;
    color: #9FFFCB;
    &:hover { 
      color: white;
     }
  }
  .navbar-brand {
    font-size: 1.4em;
    color: #D6CC10;
    &:hover { color: white; 
    
    };
    font-weight:bold;
  }
  .form-center {
    position: absolute !important;
    left: 25%;
    right: 25%;
  }
`;


export const NavigationBar = () => (
  <Styles>
    <Navbar expand="lg" className="navbar-fixed-top">
      <Navbar.Brand href="http://13.232.130.14/wordpress/">EDGE</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Item><Nav.Link href="http://13.232.130.14/wordpress/">Home</Nav.Link></Nav.Item> 
          <Nav.Item><Nav.Link href="http://13.232.130.14/wordpress/about/">About</Nav.Link></Nav.Item>
          <Nav.Item><Nav.Link href="http://13.232.130.14/wordpress/courses/">Courses</Nav.Link></Nav.Item>
          <Nav.Item><Nav.Link href="http://13.232.130.14/">References</Nav.Link></Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </Styles>
//   <div className={mystyles.navbar}>
//   <a href="#home">Home</a>
//   <a href="#news">News</a>
//   <a href="#contact">Contact</a>
// </div>
)