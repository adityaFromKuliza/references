import React from 'react';

export const SubProcesses = (props) => (
    <div className="col-md-8" id="sub-processes">
        <h4>Sub-Processes</h4>
        <p>
        BPMN provides sub-process to help with the expanding/collapsing view. A sub-process describes a detailed sequence, but it takes no more space in the diagram than a task. Both tasks and sub-processes are part of the activities class and are therefore represented as rectangles with rounded corners. The only difference is the plus sign in the rectangle indicates a stored detailed sequence for the sub-process.
        </p>
    </div>

)