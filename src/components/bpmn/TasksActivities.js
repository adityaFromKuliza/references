import React from 'react';

export const TasksActivities = (props) => (
    <div className="col-md-8" id="tasks-activities">
        <h4>Tasks/Activities</h4>
        <p>
            <b>Activities- </b> A particular task performed by a person or system. It’s shown by a rectangle with rounded corners. They can be detail oriented with loops, transactions and other instances.
There are two types of activities: Task, Sub-Process and Call activity.
<br />
            <b>Tasks- </b> A task is the fundamental business process element as it defines a unit of work. When a model/flow chart cannot be divided or broken down further, a task is used.
<br /><img src={'https://drive.google.com/uc?export=view&id=1iiJWFlfMRlSNlY6_JTTxSYov1dBSXQLH'} alt="boohoo" className="img-responsive" />
            <br />BPMN further divides tasks as a User task and a Manual task for tasks done by a person.

          <ol>
                <li>
                    <b>USER TASK</b> - It is depicted with a rounded rectangle with a user (person) icon on the top-left corner. User task represents a flow executed by a person with the assistance of a process-aware information system. For example, in case of handling orders, user task is used for order approval which is done by the buyer through a shopping system which is a software application.
            <br /><img src={'https://drive.google.com/uc?export=view&id=10ScliTvl6w47uNfJReWKBgCF9zD7KsxJ'} alt="boohoo" className="img-responsive" />
            </li>
                <li>
                    <b>MANUAL TASK</b>- Manual tasks are performed without the help of any application. This task is depicted with a small hand icon in the upper left corner. These tasks cannot be envisioned by a User Interface, hence is done without the knowledge of the system.
            <br /><img src={'https://drive.google.com/uc?export=view&id=1l6ZBeJu1a0TcfWXhnP9J5Xmy_srAR6cs'} alt="boohoo" className="img-responsive" />
                </li>
                </ol>
                Tasks without human interaction: Service task, Script task, and Business Rule task
                <ol>
                <li><b>SERVICE TASK</b>: Service task is used when an external service is required to perform a task. It could be a web service or an automated application of sort. They are mostly used to perform some sort of technical task. Most custom tasks such as the Email task or the HTTP Task are specialized versions of Service Tasks. 
<br /><img src={'https://drive.google.com/uc?export=view&id=1HVBpgF6Ymu4aVVwb-oB9iuF4lfnl9Drk'} alt="boohoo" className="img-responsive" />
                </li>
                <li>
                    <b id="script-task">SCRIPT TASK</b> - A Script Task is executed locally by a business process model. The task defines a script that the model can analyse. When the task begins, the model will execute the script. The Task will be completed when the script is followed and completed. They are mainly used to perform simple calculations or operations.
<br /><img src={'https://drive.google.com/uc?export=view&id=1ap3B8Ep4NS8pNzSMqXcDhPWi2TAszbTQ'} alt="boohoo" className="img-responsive" />
                </li>
                <li>
                    <b>RULE TASK</b> - rule tasks or business rule task is depicted as a rounded rectangle with a table icon in the top-left corner. It is solely used to apply business rules.
<br /><img src={'https://drive.google.com/uc?export=view&id=1sub_nNhQ2R9uXt9nTOVWJue2poHEy-Wa'} alt="boohoo" className="img-responsive" />
                </li>
                </ol>
                Tasks that require message exchange: Send task and Receive task 
                <ol>
                <li>
                    <b>SEND TASK</b> - Send task is used to send a message to an external participant in another pool. Once the message has been sent, the task is completed.
<br /><img src={'https://drive.google.com/uc?export=view&id=1qihx9fZ02kdW-uH3M_x47x-bqvDnZaTy'} alt="boohoo" className="img-responsive" />
                </li>

                <li>
                    <b>RECEIVE TASK</b> - Receive task is used to wait for a message to arrive from an external participant; the task is completed once the message has been received.

<br /><img src={'https://drive.google.com/uc?export=view&id=1DdhX1f8kCme1MDqMZBoRInUYGy81fDUf'} alt="boohoo" className="img-responsive" />
                </li>
                </ol>
                <h4>Call Activity</h4>
                A BPMN call activity references an activity in a process that is external to the current process definition. It allows one to create a process definition that can be reused in multiple other process definitions.
                <br /><img src={'https://drive.google.com/uc?export=view&id=1oVqlxViRHXPX_QH7k7R3yVMu3kmuELXD'} alt="boohoo" className="img-responsive" />
        </p>
    </div>

)