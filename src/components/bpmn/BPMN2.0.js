import React from 'react';
export const BPMN2 = (props) => (
        <div className="col-md-8" id="bpmn-2">
        <h4>BPMN 2.0</h4>
        <p>
        BPMN 2.0 is the biggest modification of BPMN since its establishment. It obtained the definition of a meta-model (model of a model). Because of the version 2.0 updates, the number of elements increased from 55 elements to 116. 
BPMN 2.0 was designed to help stop confusion with process maps whether an employee or consultant is trying to figure it out. Part of the reason is the utilization of standardized symbols. In a place where business process automation is put to use, BPMN 2.0 has an outstanding value for helping members to understand the process without vast knowledge while also permitting to resolve issues before moving onto the next steps. 
Therefore, the objective of BPMN 2.0 is to use modelling for improvement in productivity. Further, it offers a standard notation that does not require intensive training to understand. 

        </p>      
         </div>
  
)