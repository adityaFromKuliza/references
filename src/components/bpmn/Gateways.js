import React from 'react';

export const Gateways = (props) => (
    <div className="col-md-8" id="gateways">
        <h4>Gateways</h4>
        <p>
        Gateway is a point that can adjust the path based on conditions or events. They are represented as diamond shaped icons.  They control how sequence flows communicate and enable branching and merging of paths in a diagram.
Basically, displaying the ‘decision points’ is the purpose of Gateways.
These gateways provide users with a clearer understanding of converging and diverging of sequence flows. A BPMN user will not need to apply a gateway unless a flow specifically needs to be controlled.
Some types of gateways – exclusive gateway, inclusive gateway, parallel gateway and event based gateway.

<br />

          <ol>
                <li>
                    <b>Exclusive gateway</b> - Exclusive gateway is used to control process flow based on the given data. Each outgoing flow which is connected from the gateway corresponds to a condition. 
Exclusive gateways can be represented as either an ‘X’ symbol or left blank within the diamond shape.
Exclusive gateway can be used only for one sequence flow based on value of property 
            <br /><img src={'https://drive.google.com/uc?export=view&id=1YstJQWQme5X3cW6ix3d9hTzp_t8VT0_z'} alt="boohoo" className="img-responsive" />
            <br /><img src={'https://drive.google.com/uc?export=view&id=1TvQAZ70S-Ai8jF4o0SbbF98one5hRdea'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
            <br/>In the above example, the submit invoice is the sequence flow chosen by the exclusive gateway. Only two of the flows have conditions to go to the CFO and Finance Director but the third flow has no conditions and will be selected by default if the other two conditions are evaluated to false.
            </li>
                <li>
                    <b>Inclusive gateway</b>- Unlike exclusive gateway, the conditions of all outgoing flow are evaluated. 
Every sequence flow with a condition that evaluates to true, is followed in parallel, creating one concurrent execution for each sequence flow.
            <br /><img src={'https://drive.google.com/uc?export=view&id=1yKqsIhitikPn1_N4-o8yYg0J0or578sy'} alt="boohoo" className="img-responsive"/>
            <br /><img src={'https://drive.google.com/uc?export=view&id=1H-UhnVrJb7cTwUjlG3rs0u0d8WRbhcqG'} alt="boohoo" className="img-responsive"/>
            <br/>In the above car purchasing example, the first gateway represents the control of the flow of the process along one or more paths in the model. If the car requires cleaning, it will be cleaned. If the car has to be repaired, it will be repaired. If the car needs to be both cleaned and repaired, both can happen.
The second gateway represents the reconnection of those paths and the continuation of the flow.
When reconnecting paths like this, the inclusive gateway requires that all paths that were activated must be completed before continuing in the process.
So, if the car needed only cleaning, then the car may be driven once the cleaning is done.
If the car needed to be both cleaned and repaired, the car could not be driven until both the cleaning and repairs were completed.
    
                </li>

                <li>
                    <b>Parallel gateway</b> - It is used to model the execution of parallel flows without the need of checking any conditions.
It represents two tasks in a business flow.
It visualises the execution of converging activities
It depicts a fork into multiple paths of execution or a join of multiple paths of execution.
As a point of merging, it waits for all the incoming forks to be executed successfully.

<br /><img src={'https://drive.google.com/uc?export=view&id=12N3lp-HomN0Yyit33CSoU1dQGYgw1KY1'} alt="boohoo" className="img-responsive"/>
<br /><img src={'https://drive.google.com/uc?export=view&id=1wlTUUqRx0mT8oHfGYPmUDYCht1RBdEdo'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
                </li>
                <li>
                    <b>Event based gateway</b> - They do not evaluate data.  
It waits for an event which will later be used to determine which path the process should proceed along.
The icon of this gateway shows that it inherits elements from other icons, like the diamond represents a gateway; the thin double circle around it depicts intermediate event and a pentagon which represents multiples.
Therefore, Event based gateway deals with multiple events

<br /><img src={'https://drive.google.com/uc?export=view&id=1hxylzVv0PaPnDmbNvUWLb_x4RlgC2Ou2'} alt="boohoo" className="img-responsive" />
<br /><img src={'https://drive.google.com/uc?export=view&id=1-cGXyxQgQgvUjckgFc7J_RwrS7bKRkW_'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
                </li>
                </ol>
    <h4 id="token-concept">Token Concept</h4>
    A token is a theoretical concept that is used as an aid to define the behavior of a Process. 
The behavior of BPMN elements can be defined by describing how they interact with a token as it “traverses” them. For example, a token will pass through an exclusive gateway, but continue down only one and only one of the outgoing flows.
A start event generates a token that will be consumed at one or more end events. The path of tokens should be traceable through the network of elements (events, gateways, and activities) and sequence flows, within the pool where the process is executed.
A token is created and consumed within the boundaries of a pool. A token never crosses the boundaries of the pool where it was created.

        </p>
    </div>

)