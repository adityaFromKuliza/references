import React from 'react';
import Compiler from './Compiler';

class Objects extends React.Component {
    render(){
        const exercise='Create an object employee with attributes as firstName,lastName,age,empId.\n'+
        'Store that object in employee variable and print the mobile number of this employee';
        const javascriptCode='var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};\n'+
        'execution.setVariable("emp1",person);\n'+
        'var y=execution.getVaribale("person");\n'+
        'console.log("employee\'s age is "+y.age);\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="Objects">
        <h4>Objects</h4>
        <p>
        Objects are also variables,but objects can have multiple values.
        For example storing properties of a person we can declare person object as given below.
<br />
 </p>
 <Compiler textAreaKey="txt-area-objects" exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 <p>Quiz</p>What is the mobile number of the employee"?
 <ol type="a">
     <li><input type="radio"/>7967354797</li>
     <li><input type="radio"/>8367354597</li>
     <li><input type="radio"/>8367354797</li>
 </ol>
    </div>
    
        );
    }
    


}
export default Objects;