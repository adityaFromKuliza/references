import React from 'react';
import Compiler from './Compiler';

class Strings extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkedValue:'',
            quizAnswer:'16Williams',
            right:'none',
            wrong:'none',
        };
        this.check = this.check.bind(this);
    }
    check(event){
    
        this.setState({right:'none',wrong:'none'});
        this.setState({checkedValue:event.target.value});
        if(event.target.value==this.state.quizAnswer){
        this.setState({right:'inline'});
        }else{
            this.setState({wrong:'inline'});
        }
    }
    render(){
        const exercise='Consider the following sentence '+
        '"How much wood would a woodchuck chuck if a woodchuck could chuck wood?\n'+
        'He would chuck, he would, as much as he could, and chuck as much wood\n'+
        'As a woodchuck would if a woodchuck could chuck wood"\n'+
        'and replace wood with timber and chuck with throw';
        const javascriptCode='var newVariableX="Nothing is impossible";\n'+
        'var newVariableY = newVariableX.indexOf("i");\n'+
        'console.log("index of i  is "+newVariableY);\n'+
        'var newVariableZ = newVariableX.replace("Nothing","Everything");\n'+
        'var newVariableP = newVariableZ.replaceAll("impossible","possible");\n'+
        'console.log(p);\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="Strings">
        <h4>Strings</h4>
        <p>
        String is a sequence of characters.
        In JavaScript string can be declared either by using single quote or double quotes.
<br />
 </p>
 <Compiler exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 {/* <p>Quiz</p>What is the index value of "timber"?
 <ol type="a">
     <li><input type="radio"/>6</li>
     <li><input type="radio"/>7</li>
     <li><input type="radio"/>8</li>
 </ol> */}
 <p>Quiz</p>What is the output of 16+"Williams"?
 <ol type="a">
        <li>
            <input checked={this.state.checkedValue==='16+Williams'} type="radio" value="16+Williams" onChange={this.check} />16+Williams
            <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='16+Williams'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
            </li>
     <li>
         <input checked={this.state.checkedValue==='16Williams'} type="radio" value="16Williams"  onChange={this.check}/>16Williams
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:(this.state.checkedValue==this.state.quizAnswer)?this.state.right:'none'}}>
      <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>
         </li>
     <li>
         <input checked={this.state.checkedValue==='24'} type="radio" value="24" onChange={this.check} />24
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='24'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
         </li>
 </ol>
    </div>
        );
    }
    


}
export default Strings;