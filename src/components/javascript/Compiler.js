import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
class Compiler extends React.Component {
    constructor(...args) {
         
        super(...args);
        this.state = {
            classCode: '<script type="text/javascript">' +
                'document.getElementById("iframe-span").innerHTML' +
                '="Value of Income is ";',
            key: 0,
            count:1,
            attributes:args,
            exercise:args[0]["exercise"],
            javascriptCode:args[0]["javascriptCode"],
            note:args[0]["note"],
            textAreaKey:args[0]["textAreaKey"],
        };
        this.getClassCode = this.getClassCode.bind(this);
        this.setIframeContent = this.setIframeContent.bind(this);
        this.execute = this.execute.bind(this);

    }
    render() {
        const defaultValue=this.state.javascriptCode;
        return (
            
                <div id="compiler">
                    <p>
                        
        <b>Exercise</b>: {this.state.exercise}
        <br/><b>Note</b>: {this.state.note}
                    </p>
                    <p>Javascript Code
                 <textarea key ={this.state.textAreaKey}id={"textArea_"+this.state.textAreaKey} className="form-control rounded-0" style={{  height: '250px' }} defaultValue={defaultValue}>
                                    
                </textarea>
                    <div className="mt-2"></div>
                                <Button id={"button_"+this.state.textAreaKey} onClick={this.execute}>Run</Button>
                                
                  </p>
                  <p>          
                               
                 <iframe title="sample" key={this.state.key} id={"iframe_"+this.state.textAreaKey} className="form-control rounded-0" style={{ width: '100%', height: '250px', display:'none'}}>
                                </iframe>
                        
                        <br />


                    </p>
                </div>
        );
    }
    execute() {
        const iFramedId = document.getElementById('iframe_'+this.state.textAreaKey).contentWindow.document;
        let scriptCode = document.getElementById('textArea_'+this.state.textAreaKey).value;
        this.setIframeContent(this.getModifiedScriptCode(scriptCode), iFramedId);
    }
    setIframeContent(scriptCode, iFramedId) {   
        const classCode = this.getClassCode();
        let codeToExecute =this.getHtmlCode()+classCode + scriptCode + '</script>';
        iFramedId.open();
        iFramedId.write(codeToExecute);
        iFramedId.close();        
    }
    getClassCode() {
        return this.state.classCode;
    }
    getHtmlCode() {
        return '<span id="iframe-span"></span>';
    }
    getModifiedScriptCode(scriptCode){
        return scriptCode.replaceAll("execution","window.parent.execution");
    }
}

export default Compiler;