import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'css/navsidebar.css';
import mystyles from 'css/mystyle.module.css';
import Variables from 'components/javascript/Variables';
import Operators from 'components/javascript/Operators';
import Functions from 'components/javascript/Functions';
import Objects from 'components/javascript/Objects';
import Arrays from 'components/javascript/Arrays';
import Datatypes from 'components/javascript/Datatypes';
import Strings from 'components/javascript/Strings';
import Conditions from 'components/javascript/Conditions';
import BestPractices from 'components/javascript/BestPractices';
export const JavascriptIntroduction = (props) => (
  <div>
    
    <div id="scroll-nav" className={mystyles.sidenav}>
                    <h5>Javascript</h5>
                    <a href="#introduction">Introduction</a>
                    <a href="#best-practices">Best Practices</a>
                    <a href="#variables">Variables</a>
                    <a href="#operators">Operators</a>
                    <a href="#data-types">Data Types</a>
                    <a href="#Strings">Strings</a>
                    <a href="#functions">Funcitons</a>
                    {/* <a href="#Objects">Objects</a> */}
                    <a href="#arrays">Arrays</a>
                    <a href="#conditions">Conditions</a>
                    
                </div>
      
  <div className={mystyles.contentWrapper}>

    {/* <div className = "col-md-8"> */}
     <h4 className="col-md-8" id="introduction">Introduction</h4>
     <p className="col-md-8">
     In Lend.In <b>JavaScript</b> is primarily being used as a scripting language in <b><a href="/bpmn#script-task">script task</a></b>
     </p>
     <p className="col-md-8">
     
       </p>
       <p className="col-md-8">
        
        </p>
        <p className="col-md-8">
        
        </p>
        {/* rest of the components */}
        <BestPractices/>
        <Variables></Variables>
        <Operators></Operators>
        <Datatypes></Datatypes>
        <Strings></Strings>
        {/* <Objects></Objects> */}
        <Functions></Functions>
        <Arrays></Arrays>
        <Conditions></Conditions>
        </div>
        </div>
  
)