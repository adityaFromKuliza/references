import React from 'react';
export const Events = (props) => (
  <div className="col-md-8" id="events">
        <h4>Events</h4>
        <p>
        An event is represented with a circle containing other symbols based on event type. It activates, modifies or completes a process.
          <ol>
            <li>
            <b>Catching events</b> are events with a defined trigger. They take place once the trigger has activated or fired. These events influence the course of the process and therefore must be modeled.
            <br/>Catching events may result in:
            <ul>
            <li>The starting of the process ( Start event)</li>
            <li>The process or a process path continuing ( event)</li>
            
            <li>The task currently processed or the sub-process being canceled</li>
            <li>Another process path being used while a task or a sub-process executes</li>
            </ul>
            </li>

            <li>
   
                <b>Throwing events</b> are assumed to trigger themselves instead of reacting to a trigger. One could say that they are active compared to passive catching events. It is called throwing events for short, because the process triggers them. 
<br/>Throwing events can be:
                <ul>
            <li>Triggered during the process ( intermediate event)</li>
            <li>Triggered at the end of the process ( end event)</li>
            </ul>
</li>
<li><b>Start event</b>: the start event is denoted by a circle and has a single line. This event comes under catching events, hence it reacts to a trigger. All start events catch information. Some start events contains icons as triggers in the middle. For example, if it is a message icon, then the message arrives triggers the start of the process.
<br/><img src={'https://drive.google.com/uc?export=view&id=17gFJcn5TZjL9M-IyLTVAREhpLP0VuREr'} alt="boohoo" className="img-responsive" />
</li>
<li>
<b>Intermediate event</b> - The intermediate event circle has a double line, and the event can catch or throw information.
<br/><img src={'https://drive.google.com/uc?export=view&id=1NgfBN_pLNf7NojoaLwgniXFEeCsV0w2u'} alt="boohoo" className="img-responsive" />
</li>
<li>
<b>End event</b> - end event is represented with a single thick black line around the circle. End events come under the category of throwing because there is no process to catch after the final event. Timers, errors and messages are also some examples of end event. 
<br/><img src={'https://drive.google.com/uc?export=view&id=1yR_mwGCe74fiHxT8IBUYTD011Mml_3NI'} alt="boohoo" className="img-responsive"/>
</li>
<li>
<b>Error event</b> - Error event is used to handle the occurrence of errors during the execution of a certain activity or at a certain point in the flow of a process. This event can be a start, intermediate or an end event.
<br/><img src={'https://drive.google.com/uc?export=view&id=1fagBEFb-A3fv_osYZUvURlk8vDE6_5XF'} alt="boohoo" className="img-responsive"/>
</li>

<li>
<b>Message event</b> - The envelope icon is used to represent a message, which must be sent to or received from another participant in the process.  An intermediate event of this type can be used to initiate or start another process.

<br/><img src={'https://drive.google.com/uc?export=view&id=1pNItTCVruyVR95BMkNaKCD_gtfaY2RaL'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
<br/><img src={'https://drive.google.com/uc?export=view&id=1M8b50VeSMBqRWc0dMat374z5wO7jTxLT'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
</li>
<li>
<b>Timer event</b> - When a timer event is triggered, the process is suspended for a certain period of time. Most commonly used type of intermediate event. Suppose, the phone that the customer needs is out of stock and an order has to be given to the supplier, the process will be suspended until the phone is delivered. The timer event allows us to suspend the process.

<br/><img src={'https://drive.google.com/uc?export=view&id=1K33dBElcMViVWBx9EKnj_R3XOz6DDSQi'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
</li>
<li>
<b>Conditional event</b>- The conditional event defines an event which is triggered if a given condition is evaluated to true. It can be used as the start event of an event sub process, as an intermediate event and boundary event. The start and boundary event can be interrupting and non interrupting.

<br/><img src={'https://drive.google.com/uc?export=view&id=1EUVpc7BH2mo6oCiCIyNp7GTkrK3_LUf1'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
</li>

  </ol>
        </p>
        </div>
        // </div>
  
)