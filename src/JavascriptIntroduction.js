import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './navsidebar.css';
import mystyles from './mystyle.module.css';
import Variables from './Variables';
import Operators from './Operators';
import Functions from './Functions';
import Objects from './Objects';
import Arrays from './Arrays';
import Datatypes from './Datatypes';
import Strings from './Strings';
import Conditions from './Conditions';
import BestPractices from './BestPractices';
export const JavascriptIntroduction = (props) => (
  <div>
    
    <div id="scroll-nav" className={mystyles.sidenav}>
                    <h5>Javascript</h5>
                    <a href="#introduction">Introduction</a>
                    <a href="#best-practices">Best Practices</a>
                    <a href="#variables">Variables</a>
                    <a href="#operators">Operators</a>
                    <a href="#data-types">Data Types</a>
                    <a href="#Strings">Strings</a>
                    <a href="#functions">Funcitons</a>
                    {/* <a href="#Objects">Objects</a> */}
                    <a href="#arrays">Arrays</a>
                    <a href="#conditions">Conditions</a>
                    
                </div>
      
  <div className={mystyles.contentWrapper}>

    {/* <div className = "col-md-8"> */}
     <h4 className="col-md-8" id="introduction">Introduction</h4>
     <p className="col-md-8">
     In Lend.In <b>JavaScript</b> is primarily being used as a scripting language in <b><a href="/bpmn#script-task">script task</a></b>
     </p>
     <p className="col-md-8">
     
       </p>
       <p className="col-md-8">
        
        </p>
        <p className="col-md-8">
        
        </p>
        {/* rest of the components */}
        <BestPractices/>
        <Variables></Variables>
        <Operators></Operators>
        <Datatypes></Datatypes>
        <Strings></Strings>
        {/* <Objects></Objects> */}
        <Functions></Functions>
        <Arrays></Arrays>
        <Conditions></Conditions>
        </div>
        </div>
  
)