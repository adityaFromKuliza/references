import React from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { NavigationBar } from './components/NavigationBar';
import Routes from 'utilities/Routes';

function App() {
  return (
    <React.Fragment >  
      <NavigationBar/>
      <Routes/>
      
    </React.Fragment>
  );
}

export default App;
