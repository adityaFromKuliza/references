import React from 'react';
export const BenifitsOfBPMN = (props) => (
  <div className="col-md-8" id="benifits-of-bpmn">
        <h4>Benifits of BPMN</h4>
        <p>
          <ol>
            <li>
            Provides an easy way out for non-experts in BPMN to understand a business process diagram.
            </li>

            <li>A common language is used to ensure the user doesn’t require a technical background.
</li>
<li>The flexibility of BPMN ensures that models can be adapted to include future technologies, frameworks and processes.</li>
<li>
BPMN can boost the adaptability of businesses and cut down on expenses because of different assumptions not only in the short run.
</li>
<li>
BPMN improves collaboration between employees in different departments, companies, industries

</li>
  </ol>
        </p>
        </div>
        // </div>
  
)