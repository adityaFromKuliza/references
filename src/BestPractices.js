import React from 'react';
import Compiler from './Compiler';

class BestPractices extends React.Component {
    render(){
        const exercise='Consider the following sentence '+
        '"How much wood would a woodchuck chuck if a woodchuck could chuck wood?\n'+
        'He would chuck, he would, as much as he could, and chuck as much wood\n'+
        'As a woodchuck would if a woodchuck could chuck wood"\n'+
        'and replace wood with timber and chuck with throw';
        const javascriptCode='var newVariableX="Nothing is impossible";\n'+
        'var newVariableY = newVariableX.indexOf("i");\n'+
        'console.log("index of i  is "+newVariableY);\n'+
        'var newVariableZ = newVariableX.replace("Nothing","Everything");\n'+
        'var newVariableP = newVariableZ.replaceAll("impossible","possible");\n'+
        'console.log(p);\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="best-practices">
        <h4>BestPractices</h4>
        <p>
            <ul>
                <li>
                    Variable's name should be meaningful.
                </li>
                <li>
                    Always use camel casing while declaring identifiers.
                </li>
                <li>
                It is a good coding practice to initialize variables when you declare them.
                </li>
                <li>
                Avoid undefined values.
                </li>
            </ul>
<br />
 </p>
    </div>
        );
    }
    


}
export default BestPractices;