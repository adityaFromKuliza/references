import React from 'react';


export const Swimlanes = (props) => (
    <div className="col-md-8" id="swimlanes">
        <h4>Swimlanes</h4>
        <p>
        Swimlanes are rectangular boxes that represent participants of a business process which differentiates capabilities, roles, and responsibilities for each sub-process in a business process model (flow chart). However arranged, horizontally or vertically, it remains the same logically. Examples of swimlanes include; customer, account department, payment gateway and development team. 
There are two kinds of swimlanes – Pools and lanes.
<br /><img src={'https://drive.google.com/uc?export=view&id=1g47ByNqbVs09KwgkeAjN1Cho8228So-L'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
<br />

          <ol>
                <li>
                    <b>POOLS</b> - It is a rectangle with a label. It consists of flow elements which represent the tasks that the pool (participants) needs to perform under the process being modelled. 
            <br />
            <ul>
                <li>
                Pools further divide into expanded pool and collapsed pool
                </li>
                <li>
                Expanded pool or simply called ‘pool’ represents the internal details of the pool. Only things that matters to us should be modelled within this pool. Someone like customers are a part of collapsed pool, because what they are doing is completely under their control
                </li>
                <li>As a rule of thumb, it is said that a process diagram/model should contain exactly one expanded pool, which contains the process in scope- and if necessary one or multiple collapsed pools.</li>
                
            </ul>
            </li>
                <li>
                    <b>LANES</b>- Lanes are referred to as the subdivision of Pools. In a pool department, you may have the department head and the accountant as lanes. They are used for delegating activities to roles, systems or the departments. They should not represent individuals.
            <br /><img src={'https://drive.google.com/uc?export=view&id=1cmaVMNbaMHsUaHEJPvnzXYewslE9QEaZ'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
            <br/>In the above figure, the bank only receive credit request from customers, hence they are in the collapsed pool. The sales and the risk analyst department are represented as lanes. The flow elements (processes represented in the figure) and the lanes together make up the pool.
    
                </li>
                </ol>
<h4 id="connecting-objects">Connecting Objects</h4>
The connectors that connect the flow objects are called connecting objects. There are four kinds of connecting objects: Sequence flows, message flows, associations and data associations.
<ol>
                <li>
                    <b>Sequence pool</b> -  it is used to connect flow elements of a process. It is shown in solid line with an arrowhead. It shows the order of flow elements. It is used to connect flow elements within the same pool: either within the same pool/lane or across lanes in the same pool. 
           
            </li>
                <li>
                    <b>Message pool</b>- it is used to  connect flow elements from different pools. A message flow is shown in dotted line with an arrow head. Some examples of message that flows between pools: fax, telephone, email, letter, notice, command.
    
                </li>
                <li>
                    <b>Data associations</b>- they are used to show the flow of information between data objects, stores, inputs, and outputs. They are represented with dotted lines, which define the order of data flow. 
    
                </li>
                </ol>
    <h4 id="data">Data</h4>
    It shows the necessary data for an activity .Data can be modeled by several types of 'data' objects such as data objects, data inputs, and data outputs and data stores. 
    <ol>
                <li>
                    <b>Data object</b> -  represents information flowing through a process such as business documents, emails or letters        
            </li>
            <li>
                    <b>Data Store</b> -  A Data Store is a place where the process can read or write data, e.g., a database or a filing cabinet.  It persists beyond the lifetime of the process instance.
            </li>
            <br /><img src={'https://drive.google.com/uc?export=view&id=1xOkLGvzn8ucVk8wL2lwYThQEhaczlSMX'} alt="boohoo" className="img-responsive" style={{width:'100%',height:'100%'}}/>
            </ol>
        </p>
    </div>

)