import React from 'react';
import Compiler from './Compiler';

class Arrays extends React.Component {
    render(){
        const exercise='List 10 of your favourite food items in an array. Print the name of the 4th item in the array.\n';
        const javascriptCode='var x=["food1","food2","food3","food4"];\n'+
        'execution.setVariable("variable_name",x);\n'+
        'x[2]="newFood";\n'+
        'execution.setVariable("variable_name",x);\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="arrays">
        <h4>Arrays</h4>
        <p>
        Arrays are collection of elements each identified by an index.
        Elements in an array can be updated based on their index value.
<br />
 </p>
 <Compiler exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
    </div>
        );
    }
    


}
export default Arrays;