import React from 'react';
import Compiler from './Compiler';

class Datatypes extends React.Component {
    render(){
        const exercise='Create an object employee with attributes as firstName,lastName,age,empId.\n'+
        'Store that object in global variable "employee" and print the mobile number of this employee';
        const javascriptCode='var length = 16;\t// Number;\n'+
        'var lastName = "Johnson";\t//String\n'+
        'var newVariable = {firstName:"John", lastName:"Doe"};\t //Object\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="data-types">
        <h4>Datatypes</h4>
        <p>
        Datatypes are the most important concept.Without datatypes it is not possible to determine the behaviour of the program.
        Datatypes define the operations that can be done on the data.
<br />
For Example: When you use "+" operator with strings, then it perfroms string concatenation.
 </p>
 <Compiler textAreaKey="txt-area-dataTypes" exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 <p>Quiz</p>What is the mobile number of the employee"?
 <ol type="a">
     <li><input type="radio"/>7967354797</li>
     <li><input type="radio"/>8367354597</li>
     <li><input type="radio"/>8367354797</li>
 </ol>
    </div>
        );
    }
    


}
export default Datatypes;