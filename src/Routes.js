import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import GoalsOfBPMN from './GoalsOfBPMN';
import { BenifitsOfBPMN } from './BenifitsOfBPMN';
import { BPMN2 } from './BPMN2.0';
import { FlowObjects } from './FlowObjects';
import { Events } from './Events';
import Compiler from './Compiler';
import history from './History';
import { Home } from './Home';

import Dashboard from "./Dashboard";
import { JavascriptIntroduction } from "./JavascriptIntroduction";

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                <Route exact path="/" component={Dashboard} />
                {/* bpmn routes */}
                    <Route exact path="/bpmn" component={Home} />
                    <Route path="/goals-of-bpmn" component={GoalsOfBPMN} />
                    <Route path="/benifits-of-bpmn" component={BenifitsOfBPMN} />
                    <Route path="/bpmn-2.0" component={BPMN2} />
                    <Route path="/flow-objects" component={FlowObjects} />
                    <Route path="/events" component={Events} />
                    
                    {/* javascript routes */}
                    <Route path="/javascript" component={JavascriptIntroduction} />
                    <Route path="/try-it-out" component={Compiler} />
                </Switch>
            </Router>
        )
    }
}