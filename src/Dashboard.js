import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import history from './History';
import { Button } from 'react-bootstrap';
class Dashboard extends React.Component{
    render(){
        return (
          <React.Fragment>
            <div className="flex-column" style={{overflow:'hidden'}}>
            <div className="row">
            <img className="" src="http://13.232.130.14/wordpress/wp-content/themes/edubin/assets/images/header.jpg" alt="Card image cap"/>
            </div>
            </div>
            <div className="container mt-5 mb-5">
            
                <div className="row">

                    {/* BPMN */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>

  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/10/flow-chart-1.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">BPMN</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/bpmn')}>Know more</Button>
  </div>
 
</div>
</div>

                    {/* Javascript */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/10/javascript-2.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">Javascript</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/javascript')}>Know more</Button>
  </div>
</div>
                    </div>

                    {/* IT-essentials */}
<div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/11/computer-1.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">IT-essentials</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Know more</Button>
  </div>
</div>
                    </div>

                    {/* JSON */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/11/json-file-3.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">JSON</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Know more</Button>
  </div>
</div>
                    </div>

                    


                </div>


                {/* 2nd row */}

                <div className="row">

                    {/* BPMN */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/10/flow-chart-1.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">BPMN</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Go</Button>
  </div>
</div>
</div>

                    {/* Javascript */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/10/javascript-2.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">Javascript</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Go</Button>
  </div>
</div>
                    </div>

                    {/* IT-essentials */}
<div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/11/computer-1.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">IT-essentials</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Go</Button>
  </div>
</div>
                    </div>

                    {/* JSON */}
                    <div className="col-md-3 p-3">

                    <div className="card ml-xl-5 " style={{width: '100%'}}>
  <img className="card-img-top" src="http://13.232.130.14/wordpress/wp-content/uploads/2020/11/json-file-3.png" alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">JSON</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Button onClick={() => history.push('/')}>Go</Button>
  </div>
</div>
                    </div>

                    


                </div>
            </div>
            </React.Fragment>
      );
    }
}
export default Dashboard;
  