import React from 'react';
import Compiler from './Compiler';

class Functions extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkedValue:'',
            quizAnswer:window.parent.execution.getVariable("principalAmount")*window.parent.execution.getVariable("rateOfInterest")*window.parent.execution.getVariable("noOfYears")/100,
            right:'none',
            wrong:'none',
        };
        this.check = this.check.bind(this);
    }
    check(event){
        this.setState({right:'none',wrong:'none'});
        this.setState({checkedValue:event.target.value});
        if(event.target.value==this.state.quizAnswer){
        this.setState({right:'inline'});
        }else{
            this.setState({wrong:'inline'});
        }
    }
    render(){
        const exercise='Write a function "subtractTwoNumbers" which takes two '+
        'parameters and returns the subtraction of those arguments.\n'+
        'Write another function "calculateSimpleInterest" which takes three parameters '+
        'and returns the Simple Interest(SI). The global variables are "principalAmount,"rateOfInterest" and noOfYears"\n'+
        'Hint: SI=principalAmount*rateOfInterest*noOfYears/100';
        const javascriptCode='var newVariableX=execution.getVariable("x");\n'+
        'var newVariableY = execution.getVariable("globalVariableY");\n'+
        'var output=myFunction(4,6);\n'+
        'console.log("output "+output);\n'+
        'function myFunction(arg1,arg2){\n'+
        'return arg1+arg2;\n'+
        '}\n';
        const note='To view the output press F12 and select the console tab.';
        return(
<div className="col-md-8" id="functions">
        <h4>Functions</h4>
        <p>
        Functions define a block of code that performs a particular task.
        Because of functions it is possible to reuse the code.
<br />
 </p>
 <Compiler textAreaKey="txt-area-functions" exercise={exercise} javascriptCode={javascriptCode} note={note}></Compiler>
 <p>Quiz</p>what is the value of Simple Interest?
 <ol type="a">
 <li>
     <input checked={this.state.checkedValue==='23246.89'} type="radio" value="23246.89" onChange={this.check}/>23246.89
     <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='23246.89'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
     </li>
     <li>
         <input checked={this.state.checkedValue==='195071.45'} type="radio" value="195071.45" onChange={this.check} />195071.45
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:this.state.checkedValue==='195071.45'?this.state.wrong:'none'}}>
        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
       </svg>
         </li>
     <li>
         <input  checked={this.state.checkedValue==this.state.quizAnswer} type="radio" value={this.state.quizAnswer} onChange={this.check} />{this.state.quizAnswer}
         <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style={{display:(this.state.checkedValue==this.state.quizAnswer)?this.state.right:'none'}}>
      <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>
         </li>
 </ol>
    </div>
        );
    }
    


}
export default Functions;