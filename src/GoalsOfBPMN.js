import React from 'react';

class GoalsOfBPMN extends React.Component{

  render(){
    return(
<div className="col-md-8" id="goals-of-bpmn">
        <h4 >Goals of BPMN</h4>
        <p >
          <ol>
            <li>
            BPMN’s main goal is to be easy to translate and not force a requirement of 
            elaborate training to understand and function on it.
            </li>
            <li>Consistently readable through all levels and help 
              save time and increase efficiency.
</li>
<li>To ensure that the model is complete without requiring any additional documentation, 
to avoid usage of extra resources.</li>
<li>
To regulate the business organisation with ease and professionalism.
</li>
<li>
The BPMN diagram should represent all the information exchanged during the 
implementation of the process. 
</li>
  </ol>
        </p>
        </div>
    );
  }
  
}
export default GoalsOfBPMN;